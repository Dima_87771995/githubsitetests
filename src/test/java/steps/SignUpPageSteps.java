package steps;

import com.codeborne.selenide.Condition;
import elements.SignUpPageForm;
import io.qameta.allure.Step;

public class SignUpPageSteps {

    SignUpPageForm signUpPageForm = new SignUpPageForm();

    @Step("Write email in email field")
    public void typeEmail(String email) {
        signUpPageForm.emailField().val(email);
    }

    @Step("Write password in password field")
    public void typePassword(String password) {
        signUpPageForm.passwordField().val(password);
    }

    @Step("Registartion witn invalid data")
    public void registerWithInvalidCreds(String email, String password) {
        typeEmail(email);
        typePassword(password);
        signUpPageForm.continueButton().click();
    }

    @Step("Verify header text")
    public void getHeadingText() {
        signUpPageForm.header().waitUntil(Condition.visible, 3000).should(Condition.appear);
    }

    @Step("Verify error message")
    public void getEmailError() {
        signUpPageForm.emailError().waitUntil(Condition.visible, 3000).shouldBe(Condition.visible);
    }
}
