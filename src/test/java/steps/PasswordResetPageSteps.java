package steps;

import com.codeborne.selenide.Condition;
import elements.PasswordResetPageForm;
import io.qameta.allure.Step;
import models.GithubOperations;

public class PasswordResetPageSteps {

    PasswordResetPageForm passwordResetPageForm = new PasswordResetPageForm();

    @Step("Verify reset header text")
    public void verifyHeadingText(GithubOperations operations) {
        passwordResetPageForm.headerResetPage(operations.getHeaderResetText()).waitUntil(Condition.visible, 3000).should(Condition.appear);
    }

    @Step("Write correct email address")
    public void writeVerifiedEMail(GithubOperations operations) {
        passwordResetPageForm.emailField().val(operations.getEmail());
    }
}
