package steps;

import com.codeborne.selenide.Condition;
import elements.MainPageForm;
import io.qameta.allure.Step;
import models.GithubOperations;

public class MainPageSteps {

    MainPageForm mainPageForm = new MainPageForm();

    @Step("Click button In on main page")
    public void clickSignInButton() {
        mainPageForm.signIn().waitUntil(Condition.visible, 3000).click();

    }

    @Step("Click button registration on main page")
    public void clickSignUpButton() {
        mainPageForm.signUp().waitUntil(Condition.visible, 3000).click();
    }

    @Step("Write email in email field")
    public void typeEmail(String email) {
        mainPageForm.emailField().val(email);
    }

    @Step("Click button Sign in Github")
    public void clickSignUpForGithub() {
        mainPageForm.signUpFormButton().click();
    }

    @Step("Registration through main page")
    public void register(GithubOperations operations) {
        typeEmail(operations.getEmail());
        clickSignUpForGithub();
    }

    @Step("Verify header text")
    public void verifyHeadingText(GithubOperations operations) {
        mainPageForm.headerMain(operations.getHeaderMainText()).waitUntil(Condition.visible, 3000).should(Condition.appear);
    }
}
