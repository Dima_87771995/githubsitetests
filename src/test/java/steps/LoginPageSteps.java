package steps;

import com.codeborne.selenide.Condition;
import elements.LoginPageForm;
import io.qameta.allure.Step;
import models.GithubOperations;

public class LoginPageSteps {

    LoginPageForm loginPageForm = new LoginPageForm();

    @Step("Write user name")
    public void typeUserName(GithubOperations operations) {
        loginPageForm.loginField().val(operations.getEmail());
    }

    @Step("Write user password")
    public void typePassword(GithubOperations operations) {
        loginPageForm.passwordField().val(operations.getPassword());
    }

    @Step("Click In button")
    public void clickInButton() {
        loginPageForm.signInButton().waitUntil(Condition.visible, 3000).click();
    }

    @Step("Get header text")
    public void verifyHeadingText(GithubOperations operations) {
        loginPageForm.headerGitHub(operations.getHeaderText()).should(Condition.appear);
    }

    @Step("Get error text")
    public String getErrorText() {
        return loginPageForm.error().getText();
    }

    @Step("Create Account click")
    public void createAccount() {
        loginPageForm.createAccLink().click();
    }

    @Step("Click forgot password button")
    public void clickForgotPassword() {
        loginPageForm.headerForgotPassword().click();
    }
}
