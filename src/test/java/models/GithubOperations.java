package models;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GithubOperations {
    private String email;
    private String password;
    private String headerText;
    private String errorMessage;
    private String headerMainText;
    private String headerResetText;
}
