package github.smoke_tests;

import io.qameta.allure.*;
import models.GithubOperations;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import steps.LoginPageSteps;
import steps.MainPageSteps;
import steps.PasswordResetPageSteps;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Feature("Github")
@Story("GIT-80035 Smoke Github")
public class VerifyForgotPasswordTest {

    MainPageSteps mainPageSteps = new MainPageSteps();
    LoginPageSteps loginPageSteps = new LoginPageSteps();
    PasswordResetPageSteps passwordResetPageSteps = new PasswordResetPageSteps();
    GithubOperations githubOperations = new GithubOperations();

    @BeforeEach
    public void preconditions(){
        open("https://github.com/");

        githubOperations
                .setEmail("tester@mail.ru")
                .setPassword("1234")
                .setHeaderText("Sign in to GitHub")
                .setErrorMessage("Incorrect username or password.")
                .setHeaderMainText(" Where the world builds software ")
                .setHeaderResetText("Reset your password");
    }

    @Test()
    @TmsLink("GIT-002")
    @Description("verifyForgotPasswordTest")
    @Severity(SeverityLevel.NORMAL)
    public void verifyForgotPasswordTest() {
        mainPageSteps.verifyHeadingText(githubOperations);
        mainPageSteps.clickSignInButton();
        loginPageSteps.verifyHeadingText(githubOperations);
        loginPageSteps.typeUserName(githubOperations);
        loginPageSteps.typePassword(githubOperations);
        loginPageSteps.clickInButton();
        String error = loginPageSteps.getErrorText();
        assertEquals(githubOperations.getErrorMessage(), error);
        loginPageSteps.clickForgotPassword();
        passwordResetPageSteps.verifyHeadingText(githubOperations);
        passwordResetPageSteps.writeVerifiedEMail(githubOperations);
    }
}
