package github.smoke_tests;

import io.qameta.allure.*;
import models.GithubOperations;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import steps.LoginPageSteps;
import steps.MainPageSteps;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Feature("Github")
@Story("GIT-80035 Smoke Github")
public class InvalidAutorizationTest {

    MainPageSteps mainPageSteps = new MainPageSteps();
    LoginPageSteps loginPageSteps = new LoginPageSteps();
    GithubOperations githubOperations = new GithubOperations();

    @BeforeEach
    public void preconditions(){
        open("https://github.com/");

        githubOperations
                .setEmail("testermail.ru")
                .setPassword("1234")
                .setHeaderText("Sign in to GitHub")
                .setErrorMessage("Incorrect username or password.")
                .setHeaderMainText(" Where the world builds software ");
    }

    @Test()
    @TmsLink("GIT-001")
    @Description("verifyEmailAutorization")
    @Severity(SeverityLevel.NORMAL)
    public void verifyEmailAutorization() {
        mainPageSteps.verifyHeadingText(githubOperations);
        mainPageSteps.clickSignInButton();
        loginPageSteps.verifyHeadingText(githubOperations);
        loginPageSteps.typeUserName(githubOperations);
        loginPageSteps.typePassword(githubOperations);
        loginPageSteps.clickInButton();
        String error = loginPageSteps.getErrorText();
        assertEquals(githubOperations.getErrorMessage(), error);
    }
}
