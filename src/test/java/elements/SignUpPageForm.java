package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class SignUpPageForm {

    /**
     * Заголовок: Welcome to GitHub
     *
     * @return
     */
    public SelenideElement header() {
        return $(By.xpath("//span[text()='Welcome to GitHub!']"));
    }

    /**
     * Поле: email
     *
     * @return
     */
    public SelenideElement emailField() {
        return $(By.xpath("//input[@id = 'email']"));
    }

    /**
     * Поле: password
     *
     * @return
     */
    public SelenideElement passwordField() {
        return $(By.xpath("//input[@id = 'password']"));
    }

    /**
     * Кнопка: continue
     *
     * @return
     */
    public SelenideElement continueButton() {
        return $(By.xpath("[type='button']"));
    }

    /**
     * Ошибка: ошибка в email
     *
     * @return
     */
    public SelenideElement emailError() {
        return $(By.xpath("//p[@id='email-err']"));
    }
}
