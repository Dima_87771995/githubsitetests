package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PasswordResetPageForm {

    /**
     * Заголок: На странице сброс пароля
     *
     * @return
     */
    public SelenideElement headerResetPage(String text) {
        return $(By.xpath("//div[contains(@class,'auth-form-header')]/h1[text()='" +
                text + "']"));
    }

    /**
     * Поле: email
     *
     * @return
     */
    public SelenideElement emailField() {
        return $(By.id("email_field"));
    }
}
