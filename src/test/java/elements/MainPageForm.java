package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MainPageForm {

    /**
     * Кнопка: Войти
     *
     * @return
     */
    public SelenideElement signIn() {
        return $(By.xpath("//a[@href='/login']"));
    }

    /**
     * Кнопка: Зарегистрироваться
     *
     * @return
     */
    public SelenideElement signUp() {
        return $(By.xpath("//a[text() = 'Sign up']"));
    }

    /**
     * Поле: Ввод email
     *
     * @return
     */
    public SelenideElement emailField() {
        return $(By.xpath("//input[@id = 'user_email']"));
    }

    /**
     * Кнопка: Вход через email
     *
     * @return
     */
    public SelenideElement signUpFormButton() {
        return $(By.xpath("//button[@type = 'submit']"));
    }

    /**
     * Заголок: На главной странице
     *
     * @return
     */
    public SelenideElement headerMain(String text) {
        return $(By.xpath("//div[contains(@class,'ml-md-n3')]/h1[text()='" +
                text + "']"));
    }
}
