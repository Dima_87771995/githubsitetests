package elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginPageForm {

    /**
     * Поле: Логин
     *
     * @return
     */
    public SelenideElement loginField() {
        return $(By.xpath("//input[@id = 'login_field']"));
    }

    /**
     * Поле: Пароль
     *
     * @return
     */
    public SelenideElement passwordField() {
        return $(By.xpath("//input[@id = 'password']"));
    }

    /**
     * Кнопка: Войти
     *
     * @return
     */
    public SelenideElement signInButton() {
        return $(By.xpath("//input[@name = 'commit']"));
    }

    /**
     * Заголовок: Github
     *
     * @return
     */
    public SelenideElement headerGitHub(String text) {
        return $(By.xpath("//div[contains(@class, 'auth-form-header')]/h1[text()='" +
                text + "']"));
    }

    /**
     * Ошибка
     *
     * @return
     */
    public SelenideElement error() {
        return $(By.xpath(".//*[@id='js-flash-container']//div[contains(@class,'container')]"));
    }

    /**
     * Кнопка: Создать аккаунт
     *
     * @return
     */
    public SelenideElement createAccLink() {
        return $(By.xpath("//a[text()='Create an account']"));
    }

    /**
     * Заголовок: Забыли пароль?
     *
     * @return
     */
    public SelenideElement headerForgotPassword() {
        return $(By.xpath("//div[contains(@class,'position-relative')]" +
                "/a[text()='Forgot password?']"));
    }

}
